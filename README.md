# Telegram Sticker Bot
requires node and npm https://nodejs.org/en/download/  
open cmd and navigate to bot folder and run: npm install  
-> should install node dependencies inside node_modules (telegraf and telegraf-throttler)  
run bot by typing in terminal inside bot folder: node main.js  
  
Recommended:  
install pm2: npm install pm2 -g  
start process: pm2 start main.js --name yourbotname --restart-delay delayinms  
auto restart: pm2 startup  
