const {Telegraf} = require('telegraf');
const { telegrafThrottler } = require('telegraf-throttler');
const fs = require('fs');

const token = '' //telegram bot token

const regex = new RegExp(/something|something else/i) 

const sticker = fs.readFileSync(__dirname + '/sticker.webp')  // replace sticker with your sticker file name; sticker needs to be in .webp format https://image.online-convert.com/convert-to-webp

const bot = new Telegraf(token);
bot.use(telegrafThrottler());

bot.catch((err, ctx) => {
  console.log(`Encountered an error for ${ctx.updateType}`, err)
})

//to reply with sticker 
function send(ctx,src){
  ctx.replyWithSticker({source: src})
}
/*
just copy the next code and add regular expression
bot.hears(regex, (ctx) => {
  send(ctx, sticker)
});
*/
bot.hears(regex, (ctx) => {
  send(ctx, sticker)
});

//reply to photo
bot.on('photo', (ctx) =>{
  if(typeof ctx.message.caption !== 'undefined'){
        if(regex.test(ctx.message.caption)){
          send(ctx, sticker)
        }
  }
})
bot.launch()